FROM python:3.7-alpine

RUN apk add postgresql postgresql-contrib postgresql-client postgresql-dev gcc
RUN apk add python3-dev musl-dev linux-headers pcre-dev
RUN apk add ttf-dejavu ttf-droid ttf-freefont ttf-liberation ttf-ubuntu-font-family

ADD requirements.txt /opt/stationary/
WORKDIR /opt/stationary/
RUN pip3 install -r requirements.txt

ADD . /opt/stationary/

CMD ["/bin/bash", "./run.sh"]
