python3 ./manage.py migrate
python3 ./manage.py collectstatic --noinput

uwsgi --chdir=/opt/stationary \
	    --module=stationary.wsgi:application \
	    --env DJANGO_SETTINGS_MODULE=stationary.settings \
      --http-socket=0.0.0.0:8889 \
	    --processes=5 \
      --max-requests=5000 \
      --check-static=/opt/stationary/static/ \
      --static-map /static=/opt/stationary/static \
      --buffer-size=32768 \
      --enable-threads
