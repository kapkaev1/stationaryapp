# stationaryapp

```

Авторизация терминала
POST /api/login/

username: TERM_EMAIL
password: TERM_PASSWORD

Получаем ACCESS_TOKEN

GET Поиск пользователя по карте
/api/user?card_id=123

Получаем ID юзера из ответа

POST /api/measure/results

В заголовке указываем токен Authorization: "Token <ACCESS_TOKEN>"

user: <ID>  - юзера из предидущего ответа
params:{"oxy": {"fill": "5", "spo2": "91", "pulse": "86"}, "temperature": "36,6"} - JSON структура замера
teminal_uuid:efdcd9af-080d-49f9-ba80-f10afe6485b4 -- UUID идентификатор терминала (UUID системы)
lat:37.622504 - Latitude (широта)
lon:55.753215 - Longitude (долгота)
video: видеофайл

В ответ получаем ID замера


```