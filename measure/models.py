from django.db import models

from django.contrib.postgres.fields import JSONField
from rest_framework.fields import empty
from user.models import User, Group
from terminal.models import Terminal

class MeasureType(models.Model):
    STATIONARY = 'st'
    MOBILE = 'mb'
    TYPES = (
        (STATIONARY, 'Стационарный'),
        (MOBILE, 'Мобильный')
    )
    type = models.CharField(max_length=3, verbose_name='Тип замера')
    description = models.CharField(max_length=100, verbose_name='описание типа замера')

    class Meta:
        verbose_name = 'Тип измерения'
        verbose_name_plural = 'Типы измерений'


class Measure(models.Model):
    user = models.ForeignKey(User, related_name='measures', on_delete=models.CASCADE, verbose_name='Пользователь')
    datetime = models.DateTimeField(verbose_name='Дата измерения', blank=True, auto_now_add=True)
    terminal_serial = models.CharField(max_length=100, verbose_name='Серийный номер', null=True, blank=False)
    # terminal = models.ForeignKey(Terminal, related_name='terminal',
    #                              on_delete=models.PROTECT, verbose_name='Терминал', null=True, blank=True)
    params = JSONField(verbose_name='Данные измерения', default=dict)
    lat = models.FloatField(verbose_name='Latitude (широта)', null=True, blank=True)
    lon = models.FloatField(verbose_name='Longitude (долгота)', null=True, blank=True)
    video = models.FileField(upload_to='measures', verbose_name='Видео измерения')

    class Meta:
        verbose_name = 'Измерение'
        verbose_name_plural = 'Измерения'


class Instruction(models.Model):
    text = models.TextField(verbose_name='Текст Инструкции', null=False, blank=False)
    data = JSONField(verbose_name='Данные инструкции', null=True, blank=True, default=dict)
    group = models.ForeignKey(Group, verbose_name='Группа исполнителей', on_delete=models.PROTECT)
    datetime = models.DateTimeField(verbose_name='Дата измерения', blank=True, auto_now_add=True)

    def __str__(self):
        return "{}".format(self.id)

    class Meta:
        verbose_name = 'Инструкции'
        verbose_name_plural = 'Инструкция'


class AcceptedInstruction(models.Model):
    user = models.ForeignKey(User, blank=False, null=False, on_delete=models.CASCADE)
    instruction = models.ForeignKey(Instruction, blank=False, null=False, on_delete=models.PROTECT)
    accepted = models.BooleanField( verbose_name="Согласие с инструкцией")
    datetime = models.DateTimeField(verbose_name='Дата измерения', blank=True, auto_now_add=True)

    def __str__(self):
        return "{} {}".format(self.user.username, self.datetime)

    class Meta:
        verbose_name = 'Согласие с инструкцией'
        verbose_name_plural = 'Согласие с инструкцией'
