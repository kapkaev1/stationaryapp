from django.contrib import admin

from measure.models import MeasureType, Measure, Terminal, AcceptedInstruction, Instruction


class InstructionAdmin(admin.ModelAdmin):
    list_display = ('datetime', 'group')


class AcceptedInstructionAdmin(admin.ModelAdmin):
    list_display = ('user', 'instruction', 'accepted', 'datetime')
    list_filter = ('instruction', 'accepted')


class MeasureAdmin(admin.ModelAdmin):
    list_display = ('user', 'datetime', 'terminal_serial', 'lat', 'lon', 'video')


admin.site.register(Measure, MeasureAdmin)
admin.site.register(MeasureType)
admin.site.register(Terminal)
admin.site.register(Instruction, InstructionAdmin)
admin.site.register(AcceptedInstruction, AcceptedInstructionAdmin)
