from rest_framework import mixins, filters
from rest_framework.permissions import IsAuthenticated, BasePermission
from rest_framework.viewsets import GenericViewSet
from django.views.generic import View
from django.shortcuts import render

from measure.models import Measure, AcceptedInstruction, Instruction
from measure.serializers import MeasureSerializer, InstructionSerializer


class MeasureView(mixins.ListModelMixin, mixins.CreateModelMixin, GenericViewSet):
    queryset = Measure.objects.all()
    serializer_class = MeasureSerializer
    permission_classes = (IsAuthenticated,)


class AcceptedInstructionView(mixins.CreateModelMixin, GenericViewSet):
    queryset = AcceptedInstruction.objects.all()
    serializer_class = InstructionSerializer
    permission_classes = (IsAuthenticated,)


class InstructionView(View):
    def get(self, request, id):
        """Рендеринг инструкции"""
        instruction = Instruction.objects.get(pk=id)
        return render(request, 'instruction.html', context={'instruction': instruction})
