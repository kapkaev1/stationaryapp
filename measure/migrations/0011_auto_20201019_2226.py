# Generated by Django 2.2.6 on 2020-10-19 22:26

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('measure', '0010_auto_20201019_2211'),
    ]

    operations = [
        migrations.AlterField(
            model_name='acceptedinstruction',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
