# Generated by Django 2.2.6 on 2020-06-13 12:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('measure', '0002_auto_20200426_1504'),
    ]

    operations = [
        migrations.RenameField(
            model_name='measure',
            old_name='measure_datetime',
            new_name='datetime',
        ),
        migrations.RenameField(
            model_name='measure',
            old_name='measure_uuid',
            new_name='teminal_uuid',
        ),
        migrations.RemoveField(
            model_name='measure',
            name='location',
        ),
        migrations.RemoveField(
            model_name='measure',
            name='measure_by',
        ),
        migrations.RemoveField(
            model_name='measure',
            name='terminal',
        ),
        migrations.RemoveField(
            model_name='measure',
            name='type',
        ),
        migrations.AddField(
            model_name='measure',
            name='lat',
            field=models.FloatField(null=True, verbose_name='Latitude (широта)'),
        ),
        migrations.AddField(
            model_name='measure',
            name='lon',
            field=models.FloatField(null=True, verbose_name='Longitude (долгота)'),
        ),
    ]
