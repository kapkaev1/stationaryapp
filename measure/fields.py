from rest_framework.serializers import CharField, ValidationError

from measure.models import Terminal


class TerminalField(CharField):

    def to_internal_value(self, data):
        terminal = Terminal.objects.filter(serial_number=data).first()
        if not terminal:
            raise ValidationError('Such terminal does not exist')
        return terminal

    def to_representation(self, value):
        return value.serial_number
