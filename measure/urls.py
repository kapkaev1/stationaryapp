from rest_framework.routers import SimpleRouter
from measure.views import MeasureView, AcceptedInstructionView, InstructionView
from django.urls import path, include

router = SimpleRouter(trailing_slash=False)
router.register('results', MeasureView, base_name='measures')
router.register('instruction', AcceptedInstructionView, base_name='instruction')

urlpatterns_view = [
    path('instruction/<int:id>/', InstructionView.as_view(), name='instruction_html'),
]

urlpatterns = router.urls + urlpatterns_view
