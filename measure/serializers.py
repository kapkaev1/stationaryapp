from rest_framework.serializers import ModelSerializer

from measure.models import Measure, AcceptedInstruction


class MeasureSerializer(ModelSerializer):

    class Meta:
        model = Measure
        fields = ('id', 'user', 'terminal_serial', 'lat', 'lon', 'video', 'params')

    def create(self, validated_data):
        measure_data = validated_data
        measure_data["terminal_serial"] = self.context["request"].jwt.get('terminal_serial')
        measure = Measure.objects.create(**measure_data)
        return measure


class InstructionSerializer(ModelSerializer):
    class Meta:
        model = AcceptedInstruction
        fields = ('id', 'user', 'instruction', 'accepted')
