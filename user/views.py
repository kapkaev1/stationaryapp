from django.http import JsonResponse
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import mixins, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet
from rest_framework import filters

from user.models import User, Group, UserLogTime
from user.serializers import UserSerializer, UserLogTimeSerializer
import requests
from random import randint
from datetime import datetime, timedelta, time


class UserLog(mixins.ListModelMixin, mixins.CreateModelMixin, GenericViewSet):
    # queryset = User.objects.filter(groups__name='user').all()
    serializer_class = UserLogTimeSerializer

    # permission_classes = (,)

    # filter_backends = (DjangoFilterBackend, filters.SearchFilter)
    # filterset_fields = ('card_id',)

    def get_queryset(self):
        today = datetime.now().date()
        tomorrow = today + timedelta(1)
        today_start = datetime.combine(today, time())
        today_end = datetime.combine(tomorrow, time())

        return UserLogTime.objects.filter(date__lte=today_end, date__gte=today_start).order_by('-date').all()


class UserView(mixins.ListModelMixin, mixins.RetrieveModelMixin, mixins.CreateModelMixin, GenericViewSet):
    # queryset = User.objects.filter(groups__name='user').all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)
    filter_backends = (DjangoFilterBackend, filters.SearchFilter)
    filterset_fields = ('card_id',)

    def get_queryset(self):
        user = User.objects.filter(card_id=self.request.GET["card_id"]).first()
        if user:
            UserLogTime.objects.create(user=user).save()

        return User.objects.filter(groups__name='user').all()


class CurrentUserView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        serializer = UserSerializer(request.user)
        return Response(serializer.data, status=status.HTTP_200_OK)


class UpdatePhotoUserView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        card_id = request.data.get('card_id')
        if not card_id:
            return JsonResponse(data={'card_id': 'required field'}, status=400)

        user = User.objects.filter(card_id=card_id).first()
        if not user:
            return JsonResponse(data={'card_id': 'user with such card does not exist'}, status=400)

        if not request.FILES.get('photo'):
            return JsonResponse(data={'photo': 'required field'}, status=400)

        new_file = request.FILES.get('photo')
        user.photo.save(new_file.name, new_file.file)
        user.save()

        return Response(UserSerializer(instance=user).data, status=status.HTTP_200_OK)


class SmsUserView(APIView):

    def post(self, request):
        sms_code = 1234

        # print("{}@telemed.ru".format(request.data.get('phone')))
        # print("{}".format(request.data.get('phone')))
        user = User.objects.filter(email="{}@telemed.ru".format(request.data.get('phone'))).first()
        if not user:
            user = User.objects.create(
                username="{}".format(request.data.get('phone')),
                email="{}@telemed.ru".format(request.data.get('phone')))
            my_group = Group.objects.get(name='user')
            my_group.user_set.add(user)
            my_group.save()

        user.set_password(sms_code)
        user.save()

        # requests.post('https://parts.gt-shop.ru/bot/sendSMS/', data={
        #     'phone': request.data.get('phone'),
        #     'message': sms_code,
        #     'key': 'meSuperSMSpass'
        # })

        return JsonResponse(data={'status': 'ok'}, status=200)

# class NewUser(mixins.CreateModelMixin, GenericViewSet):
#     """
#     Create a new User
#     """
#     serializer_class = UserSerializer
