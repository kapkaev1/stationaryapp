from django.test import TestCase

from django.test.client import Client
from rest_framework.test import APIRequestFactory
from rest_framework import status
from user.models import User
from django.contrib.auth.models import Group
import json


class UserTestCase(TestCase):

    @classmethod
    def setUpTestData(self):
        self.client = Client()
        self.factory = APIRequestFactory()

        # Создадим группы
        driver_group = Group.objects.create(name='driver')

        self.user = User.objects.create(username='driver', password='DriverMe1234', email='driver@local.ru')
        self.user.groups.set([driver_group])

    def test_driver_auth(self):
        """Animals that can speak are correctly identified"""
        response = self.client.post('/api/login/',
                                    {'username': 'driver',
                                     'password': 'DriverMe1234',
                                     })
        self.assertEqual(response.status_code, 200)
        json_data = json.loads(response.content)
        assert 'status' in json_data
        assert 'signature' in json_data
        self.terminal_signature = json_data['signature']
