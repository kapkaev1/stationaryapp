from django.contrib.auth.models import Group
from rest_framework.serializers import ModelSerializer, SerializerMethodField, EmailField, CharField, FileField, \
    ValidationError
from user.models import User, UserLogTime
from measure.models import Instruction
from django.db import IntegrityError, transaction
from stationary.settings import HOST_NAME
from django.contrib.auth.hashers import make_password
import pytz

class UserLogTimeSerializer(ModelSerializer):
    card_id = SerializerMethodField()
    datetime = SerializerMethodField()

    def get_card_id(self, obj):
        return obj.user.card_id

    def get_datetime(self, obj):
        local_tz = pytz.timezone('Europe/Moscow')
        return obj.date.replace(tzinfo=pytz.utc).astimezone(local_tz)

    class Meta:
        model = UserLogTime
        fields = (
            'id', 'card_id', 'datetime')


class UserSerializer(ModelSerializer):
    groups = SerializerMethodField()
    # terminal_serial = CharField(write_only=True)
    instruction = SerializerMethodField()
    photo_url = SerializerMethodField()
    email = SerializerMethodField()
    username = SerializerMethodField()
    password = CharField(style={'input_type': 'password'}, write_only=True)

    def get_groups(self, obj):
        return obj.groups.values_list('name', flat=True)

    def get_email(self, obj):
        return obj.email

    def get_username(self, obj):
        return obj.username

    def get_instruction(self, obj):

        instruction = Instruction.objects \
            .filter(group__name__in=obj.groups.values_list('name', flat=True)) \
            .order_by('-datetime') \
            .first()

        if instruction is None:
            instruction = Instruction.objects \
                .filter(group__name="user") \
                .order_by('-datetime') \
                .first()

        return {
            "id": instruction.id,
            "url": "{}/api/measure/instruction/{}/".format(HOST_NAME, instruction.id)
        }

    def get_photo_url(self, obj):
        if not obj.photo:
            return None
        else:
            return '{}/p34{}'.format(HOST_NAME, obj.photo.url)

    class Meta:
        model = User
        fields = (
            'id', 'email', 'password', 'username', 'first_name', 'photo_url', 'middle_name', 'last_name', 'photo',
            'groups', 'card_id',
            'instruction')

    # def get_photo(self, obj):
    #     if not obj.photo:
    #         return None
    #     else:
    #         return '{}/p34{}'.format(HOST_NAME, obj.photo.url)

    def create(self, validated_data):
        user_data = validated_data

        # user_data['email'] = user_data['email'].lower()
        # user_data['username'] = user_data['email']
        user_data["username"] = user_data["card_id"]
        user_data["email"] = "{}@telemed.ru".format(user_data["username"])
        user_exists = True
        try:
            with transaction.atomic():
                user = User.objects.create(**user_data)
                user.set_password(user_data['password'])
                user.save()

                my_group = Group.objects.get(name='user')
                my_group.user_set.add(user)
                my_group.save()

                user_exists = False
                return user

        except IntegrityError as e:
            if user_exists:
                raise ValidationError({'message': ['Адрес электронной почты уже зарегистрирован', ]})
            raise ValidationError(e.__str__())
        except ValueError as e:
            raise ValidationError(e.__str__())
