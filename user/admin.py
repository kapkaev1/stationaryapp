from django.contrib import admin

from user.models import User


class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'email', 'username', 'first_name', 'last_name', 'sex', 'card_id')


admin.site.register(User, UserAdmin)
