import uuid

from django.contrib.auth.models import AbstractUser, Group
from django.db import models
from django.utils.translation import gettext_lazy as _


class User(AbstractUser):
    MALE = 'муж'
    FEMALE = 'жен'
    GENDER = (
        (MALE, 'мужской'),
        (FEMALE, 'женский')
    )
    card_id = models.CharField(max_length=50, verbose_name='Номер карты', null=True, blank=True, unique=True)
    middle_name = models.CharField(max_length=50, verbose_name='Отчество', null=True, blank=True)
    birth_date = models.DateField(verbose_name='Дата рождения', null=True, blank=True)
    sex = models.CharField(max_length=3, verbose_name='Пол сотрудника', choices=GENDER, null=True, blank=True)
    photo = models.FileField(upload_to='users', verbose_name='Фото сотрудника', null=True, blank=True)
    email = models.EmailField(_('email address'), null=True)

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return self.username


class UserLogTime(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, verbose_name='Пользователь', related_name="user")
    date = models.DateTimeField(auto_now_add=True, blank=True, verbose_name='Время авторизации')

    class Meta:
        verbose_name = 'Время авторизации'
        verbose_name_plural = 'Время авторизации'
