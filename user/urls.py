from django.urls import path
from rest_framework.routers import SimpleRouter
from user.views import UserView, CurrentUserView, SmsUserView, UserLog, UpdatePhotoUserView

router = SimpleRouter(trailing_slash=False)
router.register('', UserView, base_name='Users')
router.register('log/', UserLog, base_name='user_log')
# router.register('new/', NewUser, base_name='NewUser')

urlpatterns_custom = [
    path('info/', CurrentUserView.as_view()),
    path('update_photo/', UpdatePhotoUserView.as_view()),
    path('sms/', SmsUserView.as_view())
]

urlpatterns = router.urls + urlpatterns_custom
