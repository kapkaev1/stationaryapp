from rest_framework.serializers import ModelSerializer

from terminal.models import Terminal
from terminal.fields import TerminalField


class TerminalSerializer(ModelSerializer):
    serial_number = TerminalField(required=False)

    class Meta:
        model = Terminal
        fields = ('terminal_type', 'serial_number')
