from django.db import models


class Terminal(models.Model):
    terminal_type = models.CharField(max_length=50, verbose_name='Тип терминала')
    serial_number = models.CharField(max_length=50, verbose_name='Серийный номер терминала')

    class Meta:
        verbose_name = 'Терминал'
        verbose_name_plural = 'Терминалы'
