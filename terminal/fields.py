from rest_framework.serializers import CharField, ValidationError

from terminal.models import Terminal


class TerminalField(CharField):

    def to_internal_value(self, data):
        serial_number = Terminal.objects.filter(serial_number=data).first()
        if serial_number:
            raise ValidationError('Such terminal exists already')
        return data

    def to_representation(self, value):
        return value