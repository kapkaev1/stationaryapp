from rest_framework import mixins, filters
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import GenericViewSet

from terminal.models import Terminal
from terminal.serializers import TerminalSerializer


class TerminalView(mixins.CreateModelMixin, GenericViewSet):
    queryset = Terminal.objects.all()
    serializer_class = TerminalSerializer
    permission_classes = (IsAuthenticated,)