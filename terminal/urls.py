from rest_framework.routers import SimpleRouter
from terminal.views import TerminalView

router = SimpleRouter(trailing_slash=False)
router.register('register', TerminalView, basename='terminals')

urlpatterns = router.urls
