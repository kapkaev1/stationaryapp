from abc import ABC

from rest_framework_simplejwt.serializers import TokenObtainPairSerializer, TokenRefreshSerializer
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from rest_framework_simplejwt.state import token_backend
from user.serializers import UserSerializer
from user.models import User
import logging
logger = logging.getLogger(__name__)

class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        logger.warning("Login: {}".format(attrs["username"]))
        data = super().validate(attrs)
        data['user'] = UserSerializer(instance=self.user).data
        return data

    # @classmethod
    def get_token(self, user):
        token = super().get_token(user)
        token['terminal_serial'] = self.context["request"].data.get("terminal_serial")
        return token


class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer


class MyTokenRefreshSerializer(TokenRefreshSerializer):

    def validate(self, attrs):
        data = super().validate(attrs)
        user = User.objects.get(id=token_backend.decode(data['access'])['user_id'])
        data['refresh'] = attrs.get('refresh')
        data['user'] = UserSerializer(instance=user).data
        return data


class MyTokenRefreshView(TokenRefreshView):
    serializer_class = MyTokenRefreshSerializer