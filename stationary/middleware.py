import jwt
from jwt import DecodeError


class JwtPayloadToRequest:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        try:
            if request.headers.get('Authorization'):
                token = request.headers.get('Authorization').split()
                if len(token) > 1:
                    try:
                        request.jwt = jwt.decode(token[1], None, None)
                    except DecodeError:
                        request.jwt = {}
                else:
                    request.jwt = {}
            else:
                request.jwt = {}

            response = self.get_response(request)
        finally:
            pass

        # Code to be executed for each request/response after
        # the view is called.

        return response
