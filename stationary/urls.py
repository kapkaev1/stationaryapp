from django.contrib import admin
from django.conf import settings
from django.urls import path, include
from django.views.static import serve
from django.conf.urls import url

from .token import MyTokenObtainPairView, MyTokenRefreshView


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/login/', MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token_refresh/', MyTokenRefreshView.as_view(), name='token_refresh'),
    path('api/user/', include('user.urls')),
    path('api/measure/', include('measure.urls')),
    path('api/terminal/', include('terminal.urls')),
    url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
]
